﻿using TestDistanceBAirports.DB;
using static TestDistanceBAirports.Utils.Coordinate;

namespace TestDistanceBAirports.BL
{
    public class LocationBL
    {
        readonly ApplicationContext ApplicationContext;

        public LocationBL(ApplicationContext ApplicationContext)
        {
            this.ApplicationContext = ApplicationContext;
        }

        public static double GetDistanceByCoordinates(double lat1, double long1, double lat2, double long2, DistanceNotation? notation)
        {
            if (notation == null)
            {
                return Utils.Coordinate.GetDistanceByCoordinates(lat1, long1, lat2, long2);
            }

            return Utils.Coordinate.GetDistanceByCoordinates(lat1, long1, lat2, long2, notation.Value);
        }
    }
}
