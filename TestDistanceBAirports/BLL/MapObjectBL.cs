﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestDistanceBAirports.DB;
using static TestDistanceBAirports.Utils.Utils;

namespace TestDistanceBAirports.BL
{
    public class MapObjectBL
    {

        readonly ApplicationContext ApplicationContext;

        public MapObjectBL(ApplicationContext ApplicationContext)
        {
            this.ApplicationContext = ApplicationContext;
        }

        public Task<MapObject> Get(int id)
        {
            return ApplicationContext.MapObjects.FirstAsync(x => x.Id == id);
        }

        public Task<List<MapObject>> GetAllAsync()
        {
            return ApplicationContext.MapObjects.ToListAsync();
        }


        public Task<List<MapObject>> GetAll()
        {
            return ApplicationContext.MapObjects.ToListAsync();
        }

        public async Task<double> GetDistance(params int[] mapObjectsId)
        {
            var mapObjects = await ApplicationContext.MapObjects
                .Include(x => x.Location)
                .Where(x => mapObjectsId.Contains(x.Id))
                .ToListAsync();

            double distance = 0;
            for (int i = 1; i < mapObjects.Count; i++)
            {
                distance += mapObjects[i - 1].Location.GetDistance(mapObjects[i].Location);
            }

            return distance;
        }
    }
}
