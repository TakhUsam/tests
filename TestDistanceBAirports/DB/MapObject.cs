﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TestDistanceBAirports.DB
{
    public class MapObject
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [StringLength(250)]
        public string Name { get; set; }
        [StringLength(250)]
        public string Country { get; set; }
        [Required]
        public MapObjectType Type { get; set; }

        [Required]
        public int Hubs { get; set; }
        [Required]
        [Range(0, 10)]
        public byte Rating { get; set; }

        [Required]
        public int LocationId { get; set; }
        public Location Location { get; set; }

    }

    public enum MapObjectType
    {
        Airport = 0,

    }
}

