﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace TestDistanceBAirports.DB
{
    public class ApplicationContext : DbContext
    {
        public DbSet<MapObject> MapObjects { get; set; }
        public DbSet<Location> Locations { get; set; }

        public ApplicationContext(DbContextOptions<ApplicationContext> options)
            : base(options)
        {
            Database.EnsureCreated();
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            var locations = new Location[]
            {
                new Location(){Id = 1, Latitude = 42.4223, Longitude = 55.5303},
                new Location(){Id = 2, Latitude = 41.5423, Longitude = 24.5303},
                new Location(){Id = 3, Latitude = 24.1123, Longitude = 43.889},
                new Location(){Id = 4, Latitude = 40.400, Longitude = 42.400}
            };

            var mapObjects = new MapObject[]
            {
                new MapObject(){Id=1, Name="Аэропорт 1", Country="Russia", Type=MapObjectType.Airport, Hubs=7, LocationId=1 },
                new MapObject(){Id=2, Name="Аэропорт 2", Country="Russia", Type=MapObjectType.Airport, Hubs=4, LocationId=2 },
                new MapObject(){Id=3, Name="Аэропорт 3", Country="Russia", Type=MapObjectType.Airport, Hubs=2, LocationId=3 },
                new MapObject(){Id=4, Name="Аэропорт 4", Country="Russia", Type=MapObjectType.Airport, Hubs=4, LocationId=4 },
            };

            modelBuilder.Entity<Location>().HasData(locations);
            modelBuilder.Entity<MapObject>().HasData(mapObjects);

        }
    }
}

