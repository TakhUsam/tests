﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using TestDistanceBAirports.DB;
using TestDistanceBAirports.Utils;

namespace TestDistanceBAirports.DB
{
    public class Location
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public double Latitude { get; set; }
        [Required]
        public double Longitude { get; set; }

        public List<MapObject> MapObjects { get; set; }
    }

}


static class LocationHelper
{

    public static double GetDistance(this Location from, Location to)
    {
        return Coordinate.GetDistanceByCoordinates(from.Latitude, from.Longitude, to.Latitude, to.Longitude);
    }
}

