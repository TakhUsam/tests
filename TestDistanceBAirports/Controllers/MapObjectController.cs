﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using TestDistanceBAirports.BL;
using TestDistanceBAirports.DB;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace TestDistanceBAirports.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MapObjectController : ControllerBase
    {
        MapObjectBL MapObjectBL { get; set; }

        public MapObjectController(ApplicationContext applicationContext)
        {
            MapObjectBL = new MapObjectBL(applicationContext);
        }

        // GET: api/<MapObjectController>
        [HttpGet]
        public Task<List<MapObject>> Get()
        {
            return MapObjectBL.GetAll();
        }

        // GET api/<MapObjectController>/5
        [HttpGet("{id:int}")]
        public Task<MapObject> Get(int id)
        {
            return MapObjectBL.Get(id);
        }

        [HttpGet("{id:int}/distance/{id2:int}")]
        public Task<double> Get(int id, int id2)
        {
            return MapObjectBL.GetDistance(id, id2);
        }

        // Get /api/mapobject/distance?id=1&id=2
        [HttpGet("distance")]
        public Task<double> Get([FromQuery] int[] id)
        {
            return MapObjectBL.GetDistance(id);
        }
    }
}
