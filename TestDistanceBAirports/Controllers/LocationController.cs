﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using TestDistanceBAirports.BL;
using TestDistanceBAirports.DB;
using static TestDistanceBAirports.Utils.Coordinate;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace TestDistanceBAirports.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LocationController : ControllerBase
    {
        LocationBL LocationBL { get; set; }

        public LocationController(ApplicationContext applicationContext)
        {
            LocationBL = new LocationBL(applicationContext);
        }

        //https://localhost:44380/api/location?lat1=43&long1=44&lat2=44&long2=55
        //https://localhost:44380/api/location?lat1=3&long1=55&lat2=4&long2=4&notation=1
        [HttpGet]
        public double Get(double lat1, double long1, double lat2, double long2, DistanceNotation? notation)
        {
            return LocationBL.GetDistanceByCoordinates(lat1, long1, lat2, long2, notation);
        }
    }
}
