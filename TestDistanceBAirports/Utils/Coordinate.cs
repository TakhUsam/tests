﻿using System;
using System.Collections.Generic;

namespace TestDistanceBAirports.Utils
{
    public static class Coordinate
    {
        public enum DistanceNotation
        {
            KM = 0, //kilometers
            N = 1,  //nautical miles
            M = 2,  //miles
        }

        private static Dictionary<DistanceNotation, Double> notationCoefficient = new Dictionary<DistanceNotation, Double>()
        {
            {DistanceNotation.KM, 1.609344 },
            {DistanceNotation.N, 0.8684 },
            {DistanceNotation.M, 1 },
        };

        public static double GetDistanceByCoordinates(double lat1, double long1, double lat2, double long2, DistanceNotation notation = DistanceNotation.KM)
        {
            double rlat1 = Math.PI * lat1 / 180;

            double rlat2 = Math.PI * lat2 / 180;
            double theta = long1 - long2;
            double rtheta = Math.PI * theta / 180;
            double dist =
                Math.Sin(rlat1) * Math.Sin(rlat2) + Math.Cos(rlat1) *
                Math.Cos(rlat2) * Math.Cos(rtheta);

            if (dist > 1 || dist < -1)
            {
                throw new Exception("Invalid coordinates");
            }


            dist = Math.Acos(dist);
            dist = dist * 180 / Math.PI;
            dist = dist * 60 * 1.1515;


            return dist * notationCoefficient[notation];
        }
    }
}
